import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapReduceTaskTest
{
    MapDriver<LongWritable, Text, Text, Average> mapDriver;
    ReduceDriver<Text, Average, Text, Average> reduceDriver;
    static final String INPUT_STRING = "ip27 - - [24/Apr/2011:05:36:32 -0400] \"GET /images/sun_logo.gif HTTP/1.1\" 200 " +
            "1477 \"http://host3/\" \"Opera/9.80 (Windows NT 5.1; U; en) Presto/2.6.30 Version/10.62\"";
    static final String INPUT_STRING2 = "ip27 - - [24/Apr/2011:05:36:32 -0400] \"GET /images/sun_logo.gif HTTP/1.1\" 200 " +
            "123477 \"http://host3/\" \"Opera/9.80 (Windows NT 5.1; U; en) Presto/2.6.30 Version/10.62\"";
    MapReduceDriver<LongWritable, Text, Text, Average, Text, Average> mapReduceDriver;
    @Before
    public void setUp()
    {
        Map mapper = new Map();
        mapDriver = MapDriver.newMapDriver(mapper);
        Reduce reducer = new Reduce();
        reduceDriver = new ReduceDriver<Text, Average, Text, Average>();
        reduceDriver.setReducer(reducer);
    }

    @Test
    public void testMap() throws Exception {
        Average expectedAverage = new Average(1477,2);
        expectedAverage.setAverage(0.0);

        mapDriver.withInput(new LongWritable(2), new Text(INPUT_STRING));

        List<Pair<Text, Average>> actualResult =  mapDriver.run();
        Average actualAverage =  (Average)((Pair)((ArrayList) actualResult).get(0)).getSecond();

        Assert.assertEquals("ip27",((Pair) ((ArrayList) actualResult).get(0)).getFirst().toString());
        Assert.assertEquals(expectedAverage.getTotal(),actualAverage.getTotal());
        Assert.assertEquals(expectedAverage.getAverage(),actualAverage.getAverage(),5);
    }

    @Test
    public void testReduce() throws IOException
    {
        Average inputAverage = new Average(1477,2);
        inputAverage.setAverage(0.0);
        List<Average> avgList = new ArrayList<Average>();
        avgList.add(inputAverage);
        reduceDriver.withInput(new Text("testKey"),avgList);

        List<Pair<Text, Average>> actualResult =  reduceDriver.run();;
        Average actualAverage =  (Average)((Pair)((ArrayList) actualResult).get(0)).getSecond();

        Assert.assertEquals("testKey",((Pair) ((ArrayList) actualResult).get(0)).getFirst().toString());
        Assert.assertEquals(738.5, actualAverage.getAverage(),5);

    }

    @Test
    public void testMapReduceDriver() throws IOException
    {
        List<Pair<Text, Average>> actualResult = new MapReduceDriver <LongWritable, Text, Text, Average, Text, Average> ()
                .withMapper(new Map())
                .withInput(new LongWritable(2), new Text(INPUT_STRING))
                .withInput(new LongWritable(2), new Text(INPUT_STRING2))
                .withCombiner(new Combine())
                .withReducer(new Reduce())
                .run();
        Average actualAverage =  (Average)((Pair)((ArrayList) actualResult).get(0)).getSecond();

        Assert.assertEquals(62477.0, actualAverage.getAverage(),5);
    }
}
