import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class Combine extends Reducer<Text, Average, Text, Average>
{

    public void reduce(Text name, Iterable<Average> val, Context context)throws IOException, InterruptedException {
        int total=0, count=0;

        for (Average value : val){
            total+=value.total;
            count+=1;
        }
        context.write(name, new Average(total, count));

    }
}