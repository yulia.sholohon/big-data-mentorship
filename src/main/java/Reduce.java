import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class Reduce extends Reducer<Text, Average, Text, Average>
{
    Average avg =new Average();

    protected void reduce(Text key, Iterable<Average> val, Context context)throws IOException, InterruptedException
    {
        long total=0; double avgg=0;
        Average value = val.iterator().next();
        avgg=Double.valueOf(value.getTotal())/value.getCount();
        total = value.getTotal();
        avg.setAverage(avgg);
        avg.setTotal(total);
        context.write(key, avg);
    }
}


