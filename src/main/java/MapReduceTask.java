import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
//import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

public class MapReduceTask {

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();

        if (args.length != 2) {
            System.err.println("in/out missed");
            System.exit(2);
        }
        Job job = new Job();

        job.setJobName("map/reduce task");
        job.setJarByClass(MapReduceTask.class);
        job.setMapperClass(Map.class);

        job.setCombinerClass(Combine.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Average.class);
        job.setReducerClass(Reduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Average.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));

        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        FileOutputFormat.setCompressOutput(job, true);
       // FileOutputFormat.setOutputCompressorClass(job,  SnappyCodec.class);
        SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);

        job.waitForCompletion(true);

        CounterGroup counters = job.getCounters().getGroup("Browsers");
        for(Counter counter:counters){
            System.out.println(counter.getDisplayName() + ":" + counter.getValue());
        }
    }

}