import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Map extends Mapper<LongWritable, Text, Text, Average>
{
    private static final String LOGS_PATTERN = "^(\\S+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(\\S+) (\\S+) (\\S+)\" (\\d{3}) (\\d+) (.+?) \"([^\"]+|(.+?))\"";
    private static final Pattern pattern = Pattern.compile(LOGS_PATTERN);

    private static final String MOZILLA = "mozilla";
    private static final String OPERA = "opera";
    private static final String SAFARI = "safari";

    private static String BROWSERS = "Browsers";
    private Text key = new Text();

    public void map(LongWritable offSet, Text line, Context context) throws IOException, InterruptedException {

        Matcher matcher = pattern.matcher(line.toString());
        if (matcher.matches()) {
            this.key.set(matcher.group(1));

            // get substring before slash, after that if it browser name
            String lineWithBrowser = matcher.group(11);
            int index = !lineWithBrowser.contains("/") ? 0: lineWithBrowser.indexOf("/");
            String browserName = lineWithBrowser.substring(0,index);

           if(browserName.toLowerCase().startsWith(MOZILLA) ||
                    browserName.toLowerCase().startsWith(OPERA) ||
                    browserName.toLowerCase().startsWith(SAFARI))
            {
                Counter counter = context.getCounter(BROWSERS, browserName);
                counter.increment(1);
            }
            context.write(this.key, new Average(Long.valueOf(matcher.group(9)),1));
        }

    }
}