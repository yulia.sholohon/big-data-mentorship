import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


public class Average implements Writable {

    long total;
    int count;
    double average;

    public Average() {super();}

    public Average(long total, int count) {
        this.total = total;
        this.count = count;
    }

    public long getTotal() {return total;}
    public void setTotal(long total) {this.total = total;}
    public int getCount() {return count;}
    public void setCount(int count) {this.count = count;}

    public double getAverage() {return average;}
    public void setAverage(double average) {this.average = average;}


    public void readFields(DataInput dataInput) throws IOException {
        total = dataInput.readLong();
        count =dataInput.readInt();
        average = dataInput.readDouble();
    }


    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(total);
        dataOutput.writeInt(count);
        dataOutput.writeDouble(average);
    }

    public String toString()
    {
        return "," + average + "," + total;
    }

}